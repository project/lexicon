<?php

/**
 * @file
 *  Lexicon administration functions and forms.
 */

/**
 * Introductory stuff for the settings page.
 */
function lexicon_settings_page() {
  $mb_status = extension_loaded('mbstring') ? t('enabled') : t('disabled');
  $overload = ini_get('mbstring.func_overload') ? t('enabled') : t('disabled');
  drupal_set_message(t('Multibyte string support is %status; multibyte function overload is %overload.', array('%status' => $mb_status, '%overload' => $overload)));
  $output = drupal_get_form('lexicon_general_settings_form');
  return $output;
}

/**
 * Lexicon general settings form.
 */
function lexicon_general_settings_form($form, &$form_state) {
  drupal_add_js(drupal_get_path('module', 'lexicon') . '/js/lexicon.js');
  
  $form = array();

  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    );

  $options = array();
  $result = db_query('SELECT vid, name FROM {taxonomy_vocabulary} ORDER BY name');
  
  foreach ($result as $record) {
    $options[$record->vid] = filter_xss_admin($record->name);
  }
    
  if (!$options) {
    drupal_set_message(t('No vocabularies were found. Until you set up, and select, at least one vocabulary to use as a Lexicon, no lexicon is displayed and no marking of terms in the content of the website can be done.'));
  }

  $form['general']['lexicon_vids'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Which vocabularies act as lexicons.'),
    '#default_value' => variable_get('lexicon_vids', array()),
    '#options' => $options,
    '#description' => t('Select one or more vocabularies which hold terms for your lexicons.'),
    '#multiple' => TRUE,
    '#required' => FALSE,
    '#prefix' => '<div class="lexicon_checkboxes">',
    '#suffix' => '</div>',  
  );

  $form['lexicon_page'] = array(
    '#type' => 'fieldset',
    '#title' => t('Lexicon Page'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    );

  $form['lexicon_page']['lexicon_page_per_letter'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show lexicon across many smaller pages.'),
    '#default_value' => variable_get('lexicon_page_per_letter', FALSE),
    '#description' => t('Do you want to show all terms on one lexicon page or break up the lexicon into a page for each first letter (i.e. many pages).'),
    );

  $form['lexicon_page']['lexicon_separate_letters'] = array(
    '#type' => 'checkbox',
    '#title' => t('Separate letters.'),
    '#default_value' => variable_get('lexicon_separate_letters', FALSE),
    '#description' => t('Separate the terms by the first letters. This will create a large letter at the beginning of each section.'),
    );

  $form['lexicon_page']['lexicon_allow_no_description'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show lexicon terms even if there is no description.'),
    '#default_value' => variable_get('lexicon_allow_no_description', FALSE),
    '#description' => t('By default, Lexicon omits terms from the list if there is no term description. This setting overrides that. This is useful on free-tagging vocabularies that rarely get descriptions.'),
    );

  $form['lexicon_page']['lexicon_show_description'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show lexicon term descriptions on the Lexicon page.'),
    '#default_value' => variable_get('lexicon_show_description', FALSE),
    '#description' => t('Lexicon term descriptions may be large and/or include pictures, therefore the Lexicon page can take a long time to load if you include the full descriptions.'),
    );

  $form['lexicon_page']['lexicon_link_related'] = array(
    '#type' => 'checkbox',
    '#title' => t('Link related terms on the Lexicon page.'),
    '#default_value' => variable_get('lexicon_link_related', TRUE),
    '#description' => t('Do you want terms that are related to link to each other? The type of link is determined by "Clicking on a term link will" above.'),
    );

  $form['lexicon_page']['lexicon_term_class'] = array(
    '#type' => 'textfield',
    '#title' => t('Term link class'),
    '#default_value' => variable_get('lexicon_term_class', 'lexicon-term'),
    '#description' => t('This is the style class that will be used for "acronym," "abbr," and "hovertip" links. It should only be used by those with specific site standards.'),
    );

  $form['lexicon_page']['lexicon_show_edit'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show "edit" link.'),
    '#default_value' => variable_get('lexicon_show_edit', TRUE),
    '#description' => t('Determines whether or not to show an "edit term" link for each entry.'),
    );

  $form['lexicon_page']['lexicon_show_search'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show "search" link.'),
    '#default_value' => variable_get('lexicon_show_search', TRUE),
    '#description' => t('Determines whether or not to show an "search for term" link for each entry.'),
    );

  $form['lexicon_page']['lexicon_go_to_top_link'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show "go to top" link.'),
    '#default_value' => variable_get('lexicon_go_to_top_link', FALSE),
    '#description' => t('Determines whether or not to show a "go to top" link after each group of lexicon items per letter.'),
    );

  $form['lexicon_page']['lexicon_go_to_top_link_fragment'] = array(
    '#type' => 'textfield',
    '#title' => t('"Go to top" link anchor name.'),
    '#default_value' => variable_get('lexicon_go_to_top_link_fragment', 'top'),
    '#description' => t('Sets the "go to top" link anchor name to link to.'),
    );

  $form['lexicon_page']['lexicon_local_links_scroll'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use scroll animation for local links.'),
    '#default_value' => variable_get('lexicon_local_links_scroll', FALSE),
    '#description' => t('Determines whether or not to animate scrolling to sections when following local links on the page.'),
    );

  $form['mark_terms'] = array(
    '#type' => 'fieldset',
    '#title' => t('Term marking settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    );

  $form['mark_terms']['lexicon_mark_terms'] = array(
    '#type' => 'checkbox',
    '#title' => t('Mark terms in content.'),
    '#default_value' => variable_get('lexicon_mark_terms', TRUE),
    '#description' => t('Determines whether or not to mark terms in the content.'),
    );

  $form['mark_terms']['match'] = array(
    '#type' => 'fieldset',
    '#title' => t('Term matching'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    );

  $form['mark_terms']['match']["lexicon_match"] = array(
    '#type' => 'select',
    '#title' => t('Match type'),
    '#default_value' => variable_get("lexicon_match", 'b'),
    '#options' => array(
      'b' => t('Word'),
      'lr' => t('Right or left substring'),
      'l' => t('Left substring'),
      'r' => t('Right substring'),
      's' => t('Any substring'),
      ),
    '#description' => t('Choose the match type of lexicon links. "Word" means a word break must occur on both sides of the term. "Right or left" requires a word break on either side. "Left" requires a word break on the left side of the term. "Right" requires a word break on the right. "Any" means any substring will match.'),
    '#prefix' => '<div class="lexicon_radios">',
    '#suffix' => '</div>',
    );

  $form['mark_terms']['match']["lexicon_case"] = array(
    '#type' => 'select',
    '#title' => t('Case sensitivity'),
    '#default_value' => variable_get("lexicon_case", '1'),
    '#options' => array(
      t('Case insensitive'),
      t('Case sensitive')
      ),
    '#description' => t('Match either case sensitive or not. Case sensitive matches are not very resource intensive.'),
    '#prefix' => '<div class="lexicon_radios">',
    '#suffix' => '</div>',
    );

  $form['mark_terms']['match']["lexicon_replace_all"] = array(
    '#type' => 'select',
    '#title' => t('Replace matches'),
    '#default_value' => variable_get("lexicon_replace_all", 0),
    '#options' => array(
      t('Only the first match'),
      t('All matches')
      ),
    '#description' => t('Whether only the first match should be replaced or all matches.'),
    '#prefix' => '<div class="lexicon_radios">',
    '#suffix' => '</div>',
    );

  $form['mark_terms']['match']["lexicon_blocking_tags"] = array(
    '#type' => 'textarea',
    '#title' => t('Blocked elements'),
    '#default_value' => variable_get("lexicon_blocking_tags", 'abbr acronym'),
    '#cols' => 60,
    '#rows' => 1,
    '#maxlength' => 512,
    '#description' => t('Which HTML elements (tags) should not include Lexicon links;
      that is, text within these elements will not be scanned for lexicon terms.
      Enter the list separated by a space and do not include < and > characters (e.g. h1 h2).
      To use a %span element to skip text, prefix the class name with a dot (e.g. ".skipping-this").
      All "a," "code," and "pre" elements will be skipped by default.
      ', array('%span' => 'span')),
    );

  $form['mark_terms']['indicator'] = array(
    '#type' => 'fieldset',
    '#title' => t('Link style'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    );

  $click_options = array(
    t('Show only the single term.'),
    t('Advance the whole lexicon to the term.'),
    );
  $form['mark_terms']['indicator']['lexicon_click_option'] = array(
    '#type' => 'select',
    '#title' => t('Clicking on a term link will'),
    '#options' => $click_options,
    '#default_value' => variable_get('lexicon_click_option', 0),
    '#description' => t('Set the link to show only the single term or the whole lexicon.'),
    '#prefix' => '<div class="lexicon_radios">',
    '#suffix' => '</div>',
    );

  $form['mark_terms']['indicator']["lexicon_link"] = array(
    '#type' => 'select',
    '#options' => array(
      'none' => t('none'),
      'normal' => t('normal'),
      'absolute' => t('absolute'),
      ),
    '#title' => t('Link type'),
    '#default_value' => variable_get("lexicon_link", 'normal'),
    '#description' => t('You may choose no linking of terms ("none"), standard site linking ("normal"), or "absolute" links. RSS feeds need absolute links to ensure they point back to this site. If you are not providing RSS feeds, it is better to choose one of the other types.'),
    '#prefix' => '<div class="lexicon_radios">',
    '#suffix' => '</div>',
    );

  $indicator_options = array(
      'superscript' => t('Superscript'),
      'icon' => t('Icon'),
      'iconterm' => t('Icon + Term'),
      'abbr' => t('Use !type element', array('!type' => l(t('abbr'), 'http://www.w3.org/TR/html401/struct/text.html#edef-ABBR'))),
      'acronym' => t('Use !type element', array('!type' => l(t('acronym'), 'http://www.w3.org/TR/html401/struct/text.html#edef-ACRONYM'))),
      'cite' => t('Use !type element', array('!type' => l(t('cite'), 'http://www.w3.org/TR/html401/struct/text.html#edef-CITE'))),
      'dfn' => t('Use !type element', array('!type' => l(t('dfn'), 'http://www.w3.org/TR/html401/struct/text.html#edef-DFN'))),
      );

  $form['mark_terms']['indicator']['lexicon_replace'] = array(
    '#type' => 'radios',
    '#title' => t('Term Indicator'),
    '#default_value' => variable_get("lexicon_replace", 'superscript'),
    '#options' => $indicator_options,
    '#description' => t('This determines how the link to the lexicon term will be indicated. The "phrase" items are linked to the standards in case you want to study them.'),
    '#validate' => array('lexicon_indicator_intercept' => array()),
    '#prefix' => '<div class="lexicon_radios">',
    '#suffix' => '</div>',
    );

  $form['mark_terms']['indicator']["lexicon_superscript"] = array(
    '#type' => 'textfield',
    '#title' => t('Superscript'),
    '#default_value' =>  variable_get("lexicon_superscript", 'i'),
    '#size' => 15,
    '#maxlength' => 255,
    '#description' => t('If you chose "superscript" above, enter the superscript text.'),
    '#validate' => array('lexicon_indicator_intercept' => array()),
    '#prefix' => '<div class="lexicon_superscript">',
    '#suffix' => '</div>',
    );

  $form['mark_terms']['indicator']["lexicon_icon"] = array(
    '#type' => 'textfield',
    '#title' => t('Lexicon Icon URL'),
    '#default_value' => variable_get("lexicon_icon", drupal_get_path('module', 'lexicon') . '/imgs/lexicon.gif'),
    '#size' => 50,
    '#maxlength' => 255,
    '#description' => t('If you chose "icon" above, enter the URL of the lexicon icon relative to the root ("%root") of your Drupal site.', array('%root' => base_path())),
    '#prefix' => '<div class="lexicon_icon">',
    '#suffix' => '</div>',
    );

  $form['mark_terms']['indicator']['lexicon_disable_indicator'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow the user to disable lexicon links.'),
    '#default_value' => variable_get('lexicon_disable_indicator', FALSE),
    '#description' => t('Determines whether or not the individual user may disable the Lexicon indicators.'),
    );

  $form['lexicon_clear_cache_on_submit'] = array(
    '#type' => 'checkbox',
    '#title' => t('Clear the cache when settings are submitted.'),
    '#default_value' => TRUE,
    '#description' => t('Changes in the filter behaviour are only visible when the cache is flushed. This settings ensures that the cache is flushed when the settings are submitted.'),
    '#prefix' => '<div class="lexicon_clear_cache_on_submit">',
    '#suffix' => '</div>',
    );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
    '#weight' => 5,
    );

  return $form;
}

/**
 * Lexicon general settings form submission function.
 */
function lexicon_general_settings_form_submit($form, &$form_state) {
  variable_set('lexicon_vids', $form_state['values']['lexicon_vids']);
  variable_set('lexicon_disable_indicator', $form_state['values']['lexicon_disable_indicator']);
  variable_set('lexicon_click_option', $form_state['values']['lexicon_click_option']);
  variable_set('lexicon_show_edit', $form_state['values']['lexicon_show_edit']);
  variable_set('lexicon_show_search', $form_state['values']['lexicon_show_search']);
  variable_set('lexicon_go_to_top_link', $form_state['values']['lexicon_go_to_top_link']);
  variable_set('lexicon_go_to_top_link_fragment', $form_state['values']['lexicon_go_to_top_link_fragment']);
  variable_set('lexicon_local_links_scroll', $form_state['values']['lexicon_local_links_scroll']);
  variable_set('lexicon_page_per_letter', $form_state['values']['lexicon_page_per_letter']);
  variable_set('lexicon_separate_letters', $form_state['values']['lexicon_separate_letters']);
  variable_set('lexicon_allow_no_description', $form_state['values']['lexicon_allow_no_description']);
  variable_set('lexicon_show_description', $form_state['values']['lexicon_show_description']);
  variable_set('lexicon_link_related', $form_state['values']['lexicon_link_related']);
  variable_set('lexicon_term_class', $form_state['values']['lexicon_term_class']);
  variable_set('lexicon_mark_terms', $form_state['values']['lexicon_mark_terms']);
  variable_set('lexicon_match', $form_state['values']['lexicon_match']);
  variable_set('lexicon_case', $form_state['values']['lexicon_case']);
  variable_set('lexicon_replace_all', $form_state['values']['lexicon_replace_all']);
  variable_set('lexicon_blocking_tags', $form_state['values']['lexicon_blocking_tags']);
  variable_set('lexicon_link', $form_state['values']['lexicon_link']);
  variable_set('lexicon_replace', $form_state['values']['lexicon_replace']);
  variable_set('lexicon_superscript', $form_state['values']['lexicon_superscript']);
  variable_set('lexicon_icon', $form_state['values']['lexicon_icon']);

  if ($form_state['values']['lexicon_clear_cache_on_submit'] == 1) {
    _lexicon_clear_cache();
  }

  drupal_set_message(t('Configuration has been updated.'));

  return;
}

/**
 *  Lexicon alphabar settings form.
 */
function lexicon_alphabet_form($form, &$form_state) {
  global $language;
  $form = array();

  $form['alphabet'] = array(
    '#type' => 'textarea',
    '#title' => t('Enter all the letters of your alphabet, in the correct order, and in lower case.'),
    '#default_value' => implode(' ', variable_get('lexicon_alphabet', range('a', 'z'))),
    '#description' => t('Separate the letters by a blank.'),
    '#rows' => 1,
    );

  $form['digits'] = array(
    '#type' => 'textarea',
    '#title' => t('Enter all the digits of your alphabet, in the correct order.'),
    '#default_value' => implode(' ', variable_get('lexicon_digits', range('0', '9'))),
    '#description' => t("Separate the digits by a blank. If you don't want terms to start with digits, leave this blank."),
    '#rows' => 1,
    );

  $form['suppress_unused'] = array(
    '#type' => 'checkbox',
    '#title' => t('Suppress unused letters?'),
    '#default_value' => variable_get('lexicon_suppress_unused', FALSE),
    '#description' => t('This will cause unused letters to be omitted from the alphabar.'),
    );

  $ab_seps = array(
    ' ' => t('none'),
    '|' => t('vertical bar (pipe)'),
    '&bull;' => t('bullet'),
    '&#8211;' => t('en-dash (&#8211;)'),
    '&#8212;' => t('em-dash (&#8212;)'),
    '_' => t('underscore'),
    );
  $form['alphabar_separator'] = array(
    '#type' => 'radios',
    '#options' => $ab_seps,
    '#title' => t('Alphabar separator'),
    '#default_value' => variable_get('lexicon_alphabar_separator', '|'),
    '#description' => t('This is the character that will separate the letters in the alphabar.'),
    '#prefix' => '<div class="lexicon_radios">',
    '#suffix' => '</div>',
    );

  $form['alphabar_instruction'] = array(
    '#type' => 'textarea',
    '#title' => t('Alphabar instruction'),
    '#default_value' => variable_get('lexicon_alphabar_instruction', _alphabar_instruction_default()),
    '#description' => t('This is the text that will appear immediately below the alphabar.'),
    '#rows' => 1,
    );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
    '#weight' => 5,
    );

  return $form;
}

/**
 * Lexicon alphabar settings form submission function.
 */
function lexicon_alphabet_form_submit($form, &$form_state) {
  variable_set('lexicon_alphabet', explode(' ', $form_state['values']['alphabet']));
  if ($form_state['values']['digits']) {
    variable_set('lexicon_digits', explode(' ', $form_state['values']['digits']));
  }
  else {
    variable_set('lexicon_digits', array());
  }
  variable_set('lexicon_suppress_unused', $form_state['values']['suppress_unused']);
  variable_set('lexicon_alphabar_separator', $form_state['values']['alphabar_separator']);
  variable_set('lexicon_alphabar_instruction', $form_state['values']['alphabar_instruction']);

  drupal_set_message(t('Configuration saved'), 'status');
}

/**
 * Lexicon path and titles settings form.
 */
function lexicon_paths_and_titles_form($form, &$form_state) {
  $form = array();
  $vids = variable_get('lexicon_vids', array());
  $vids_setup = FALSE;
  
  $form['vids'] = array(
    '#type' => 'value',
    '#value' => $vids,
  );
    
  foreach ($vids as $vid) {
    if ($vid != 0) {
      $vids_setup = TRUE;
      
      $result = db_query('SELECT name FROM {taxonomy_vocabulary} WHERE vid = :vid',
      array(
        ':vid' => $vid,
      ));
      $vocabulary = $result->fetch();
      $vocabulary_name = filter_xss_admin($vocabulary->name);

      $form['paths_and_titles_' . $vid] = array(
      '#type' => 'fieldset',
      '#title' => t('Path and title settings for %vocabulary_name', array('%vocabulary_name' => $vocabulary_name)),
      '#collapsible' => FALSE,
      );

      $form['paths_and_titles_' . $vid]['lexicon_path_' . $vid] = array(
        '#type' => 'textfield',
        '#title' => t('The path of the lexicon for this vocabulary'),
        '#default_value' => variable_get('lexicon_path_' . $vid, 'lexicon/' . $vid),
        '#description' => t('Determines the path that is used for the lexicon page for this vocabulary. Default is: <em>%path</em>.', array('%path' => 'lexicon/' . $vid)),
        '#required' => TRUE,
        );

      $form['paths_and_titles_' . $vid]['lexicon_title_' . $vid] = array(
        '#type' => 'textfield',
        '#title' => t('The title of the lexicon for this vocabulary'),
        '#default_value' => variable_get('lexicon_title_' . $vid, $vocabulary_name),
        '#description' => t('Determines the title that is used for the lexicon page for this vocabulary. Default is: <em>%name</em>.', array('%name' => $vocabulary_name)),
        '#required' => TRUE,
        );
    }
  }
  
  if ($vids_setup) {
    $form['lexicon_clear_cache_on_submit'] = array(
      '#type' => 'checkbox',
      '#title' => t('Clear the cache when settings are submitted.'),
      '#default_value' => TRUE,
      '#description' => t('Changes in the paths and titles are only visible when the cache is flushed. This settings ensures that the cache is flushed when the settings are submitted.'),
      '#prefix' => '<div class="lexicon_clear_cache_on_submit">',
      '#suffix' => '</div>',
      );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save configuration'),
      '#weight' => 5,
      );
  }
  else {
    drupal_set_message(t('No vocabularies were found. Until you set up, and select, at least one vocabulary for Lexicon, no settings can be entered.'));
  }
  
  return $form;
}

/**
 * Lexicon paths and titles settings form submission function.
 */
function lexicon_paths_and_titles_form_submit($form, &$form_state) {
  $vids = $form_state['values']['vids'];
  
  foreach ($vids as $vid) {
    if ($vid != 0 ) {
      variable_set('lexicon_path_' . $vid, $form_state['values']['lexicon_path_' . $vid]);
      variable_set('lexicon_title_' . $vid, $form_state['values']['lexicon_title_' . $vid]);
    }
  }

  if ($form_state['values']['lexicon_clear_cache_on_submit'] == 1) {
    _lexicon_clear_cache();
  }

  drupal_set_message(t('Configuration saved'), 'status');
}

/**
 * Lexicon related terms and synonyms settings form.
 */
function lexicon_related_terms_and_synonyms_form($form, &$form_state) {
  $form = array();
  
  $vids = variable_get('lexicon_vids', array());
  $vocs = taxonomy_get_vocabularies();
  $vids_machine_names = array();
  $vids_setup = FALSE;
  $instances = field_info_instances();
  
  foreach ($vids as $vid) {
    if ($vid != 0) {
      $vids_machine_names[(int)$vid] = $vocs[(int)$vid]->machine_name;
      $vids_setup = TRUE;
    }
  }
  
  $form['vids'] = array(
    '#type' => 'value',
    '#value' => $vids,
  );
  
  foreach ($vids as $vid) {
    if ($vid != 0) {
      $vids_setup = TRUE;
      $related_terms_setup = FALSE;
      $synonyms_setup = FALSE;
      
      $options_related_terms = array( 
        "" => t("none"),
      );
      $options_synonyms = array(
        "" => t("none"),
      );
  
      foreach ($instances["taxonomy_term"][$vids_machine_names[(int)$vid]] as $field) {
        if ($field["widget"]["type"] == "options_select") {
          $options_related_terms[$field["field_name"]] = $field["label"];
          $related_terms_setup = TRUE;
        }
        if ($field["widget"]["type"] == "text_textfield") {
          $options_synonyms[$field["field_name"]] = $field["label"];
          $synonyms_setup = TRUE;
        }
      }
                  
      $result = db_query('SELECT name FROM {taxonomy_vocabulary} WHERE vid = :vid',
      array(
        ':vid' => $vid,
      ));
      $vocabulary = $result->fetch();
      $vocabulary_name = filter_xss_admin($vocabulary->name);
      
      $form['related_terms_and_synonyms_' . $vid] = array(
      '#type' => 'fieldset',
      '#title' => t('Related terms and synonyms settings for %vocabulary_name', array('%vocabulary_name' => $vocabulary_name)),
      '#collapsible' => FALSE,
      );
      
      if ($related_terms_setup) {
        $form['related_terms_and_synonyms_' . $vid]['lexicon_related_terms_field_' . $vid] = array(
         '#type' => 'select',
         '#options' => $options_related_terms,
         '#title' => t('Field for related terms'),
         '#default_value' => variable_get('lexicon_related_terms_field_' . $vid, ''),
         '#description' => t('Determines if related terms are shown and which field is used for the related terms. The default value is : <em>none</em>.'),
         '#prefix' => '<div class="lexicon_radios">',
         '#suffix' => '</div>',
         '#required' => FALSE,
        );
      }
      else {
        drupal_set_message(t('No fields for related terms for the vocabulary <em> %vocabulary_name </em> were found. Until you set up at least one related terms field for the vocabulary, no settings can be entered.', array('%vocabulary_name' => $vocabulary_name)));
      }
       
      if ($synonyms_setup) {
        $form['related_terms_and_synonyms_' . $vid]['lexicon_synonyms_field_' . $vid] = array(
         '#type' => 'select',
         '#options' => $options_synonyms,
         '#title' => t('Field for synonyms'),
         '#default_value' => variable_get('lexicon_synonyms_field_' . $vid, ''),
         '#description' => t('Determines if synonyms are shown and which field is used for synonyms. The default value is :') . ' <em>none</em>.',
         '#prefix' => '<div class="lexicon_radios">',
         '#suffix' => '</div>',
         '#required' => FALSE,
        );
      }
    }
  }
  
  if ($vids_setup) {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save configuration'),
      '#weight' => 5,
      );
  }
  else {
    drupal_set_message(t('No vocabularies were found. Until you set up, and select, at least one vocabulary for Lexicon, no settings can be entered.'));
  }
  
  return $form;
}

/**
 * Lexicon related terms and synonyms settings form submission function.
 */
function lexicon_related_terms_and_synonyms_form_submit($form, &$form_state) {
  $vids = $form_state['values']['vids'];
  foreach ($vids as $vid) {
    if ($vid != 0 ) {
      variable_set('lexicon_related_terms_field_' . $vid, $form_state['values']['lexicon_related_terms_field_' . $vid]);
      variable_set('lexicon_synonyms_field_' . $vid, $form_state['values']['lexicon_synonyms_field_' . $vid]);
    }
  }

  drupal_set_message(t('Configuration saved'), 'status');
}

/**
 * Menu call back for clear cache button on settings page function.
 */
function lexicon_clearcache_form($form, &$form_state) {
  $form = array();
  $form['text'] = array(
    '#value' => '<p>' . t('This clears the cache_filter and cache_menu table. To clear all cached data, you may use the button on the <a href="!url">Performance settings</a> page.', array('!url' => url('admin/config/development/performance'))) . '</p>',
    );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Clear cache'),
    );
  $form['#submit'][] = '_lexicon_clear_cache';

  return $form;
}