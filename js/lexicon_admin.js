
(function($) {
  function lexicon_replace_handler(event) {
    // Disable superscript field if not selected.
    if ($("input[name=lexicon_replace]:checked").val() == 'superscript') {
      $("input[name=lexicon_superscript]").parents("div.lexicon_superscript").show();
    }
    else {
      $("input[name=lexicon_superscript]").parents("div.lexicon_superscript").hide();
    }

    // Disable icon URL field if not selected.
    var selected = $("input[name=lexicon_replace]:checked").val();
    if (selected == 'icon' || selected == 'iconterm') {
      $("input[name=lexicon_icon]").parents("div.lexicon_icon").show();
    }
    else {
      $("input[name=lexicon_icon]").parents("div.lexicon_icon").hide();;
    }
  }

  // Run the javascript on page load.
  $(document).ready(function () {
    // On page load, determine the current settings.
    lexicon_replace_handler();
    // Bind the function to click events.
    $("input[name=lexicon_replace]").bind("click", lexicon_replace_handler);
  });
})(jQuery);