
Lexicon
http://www.drupal.org/project/lexicon
=====================================


DESCRIPTION
-----------
A lexicon is a stock of terms used in a particular profession, subject or style; a vocabulary. 
The Lexicon module generates one or more Lexicon pages based on terms in taxonomies and optionally 
marks and links them in the content.

The Lexicon module is a branch of the Glossary module (http://www.drupal.org/project/glossary) that 
was changed to suit the needs of a project for the Dutch government. The main reason for the changes 
is compliancy with the web guidelines of the Dutch government (http://www.webguidelines.nl).

The main differences with the Glossary module are that the Lexicon module:

    * Produces output that is valid W3C XHTML 1.0 Strict.
    * Enables showing lists of terms without marking the terms in content (this is still an option). 
      You can choose which vocabularies act as Lexicons and you don't bind them to input filters.
    * Has configurable paths and titles for each Lexicon.
    * Optionally inserts a "go to top" link for each section on the Lexicon page.
    * Optionally "scroll to" internal links on the Lexicon page.

Development of this module is sponsored by Sogeti Netherlands (http://sogeti.doethetmetdrupal.nl).


REQUIREMENTS
------------
Drupal 7.x
Taxonomy
Fields


INSTALLING
----------
1. To install the module copy the 'lexicon' folder to your sites/all/modules directory.

2. Go to admin/build/modules. Enable the Lexicon module.
Read more about installing modules at http://drupal.org/node/70151


CONFIGURING AND USING
---------------------
1. Create a taxonomy vocabulary for each Lexicon.
2. Go to admin/settings/lexicon.
3. Select the created taxonomy vocabularies you want to use as Lexicons.
4. Set the desired behavior of the Lexicon module and save the configuration.
5. Configure the alphabar and save the configuration.
6. Optionally set the paths and titles of the Lexicons and save the configuration.
7. Optionally put the suggested menu-items in the right menu and activate them.

Have fun!